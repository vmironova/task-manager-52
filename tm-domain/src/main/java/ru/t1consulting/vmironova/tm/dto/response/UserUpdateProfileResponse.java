package ru.t1consulting.vmironova.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class UserUpdateProfileResponse extends AbstractResponse {
}
